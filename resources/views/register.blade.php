<html>
    <head>
        <title>Tugas 1 - Shofari Bagus</title>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <form action="/welcome" method="POST">
            @csrf
            <label for="first_name">First name :</label><br><br>
            <input type="text" name="first_name" id="first_name"><br><br>
            <label for="last_name">Last name :</label><br><br>
            <input type="text" name="last_name" id="last_name">
            <br><br>

            <label for="gender">Gender : </label><br><br>
            <input type="radio" name="gender" id="g_male">
            <label for="g_male">Male</label><br>
            <input type="radio" name="gender" id="g_female">
            <label for="g_female">Female</label><br>
            <input type="radio" name="gender" id="g_other">
            <label for="g_other">Other</label><br>
            <br><br>

            <label for="nationality">Nationality</label><br><br>
            <select name="nationality" id="nationality">
                <option value="indonesia">Indonesian</option>
                <option value="amerika">Amerika</option>
                <option value="inggris">Inggris</option>
            </select>
            <br><br>

            <label for="language">Language Spoken</label><br><br>
            <input type="checkbox" name="language" id="l_indonesia">
            <label for="l_indonesia">Bahasa Indonesia</label>
            <input type="checkbox" name="language" id="l_english">
            <label for="l_english">English</label>
            <input type="checkbox" name="language" id="l_other">
            <label for="l_other">Other</label>
            <br><br>

            <label for="bio">Bio :</label><br><br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
            <br>
            <button type="submit">Sign Up</button>
        </form>
    </body>
</html>

@extends('layout.master')
@section('judul','Cast')
@section('konten')

<a href="/cast/create" class="btn btn-success">Tambah Cast</a> <hr>
<table class="table table-bordered">
    <thead>
      <tr>
        <th style="width: 10px">No</th>
        <th>Nama</th>
        <th>Umur</th>
        <th>Bio</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($cast as $row => $value)
        <tr>
            <td>{{$row + 1}}</td>
            <td>{{$value->nama}}</td>
            <td>{{$value->umur}}</td>
            <td>{{$value->bio}}</td>
            <td>
                <div class="row">
                    <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm mx-1">Show</a>
                <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm mx-1">Edit</a>
                <form action="/cast/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm mx-1">
                </form>
                </div>
            </td>
          </tr>
        @endforeach
    </tbody>
  </table>
  @endsection

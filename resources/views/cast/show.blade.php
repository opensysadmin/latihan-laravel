@extends('layout.master')
@section('judul','Show Cast')
@section('konten')
<table class="table table-responsive">
    <tr>
        <th>Nama</th>
        <td>:</td>
        <td>{{$cast->nama}}</td>
    </tr>
    <tr>
        <th>Umur</th>
        <td>:</td>
        <td>{{$cast->umur}}</td>
    </tr>
    <tr>
        <th>Bio</th>
        <td>:</td>
        <td>{{$cast->bio}}</td>
    </tr>
</table>
<hr>
<a href="/cast" class="btn btn-success">Kembali</a>
@endsection

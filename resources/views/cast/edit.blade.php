@extends('layout.master')
@section('judul','Edit Cast')
@section('konten')
<form action="/cast/{{$cast->id}}" method="POST">
@csrf
@method('put')
<div class="form-group">
    <label for="nama">Nama</label>
    <input type="text" name="nama" id="nama" class="form-control" value="{{$cast->nama}}">
    @error('nama')
    <div class="alert alert-danger">
        {{$message}}
    </div>
    @enderror
</div>
<div class="form-group">
    <label for="umur">Umur</label>
    <input type="number" name="umur" id="umur" class="form-control" value="{{$cast->umur}}">
    @error('umur')
    <div class="alert alert-danger">
        {{$message}}
    </div>
    @enderror
</div>
<div class="form-group">
    <label for="bio">Bio</label>
    <textarea name="bio" id="bio" cols="30" rows="10" class="form-control">{{$cast->bio}}</textarea>
    @error('nama')
    <div class="alert alert-danger">
        {{$message}}
    </div>
    @enderror
</div>
<div class="form-group">
    <button type="submit" class="btn btn-success">Simpan</button>
</div>
</form>
@endsection

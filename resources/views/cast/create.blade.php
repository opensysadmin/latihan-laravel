@extends('layout.master')
@section('judul','Tambah Cast')
@section('konten')
<form action="/cast" method="POST">
@csrf
<div class="form-group">
    <label for="nama">Nama</label>
    <input type="text" name="nama" id="nama" class="form-control">
    @error('nama')
    <div class="alert alert-danger">
        {{$message}}
    </div>
    @enderror
</div>
<div class="form-group">
    <label for="umur">Umur</label>
    <input type="number" name="umur" id="umur" class="form-control">
    @error('umur')
    <div class="alert alert-danger">
        {{$message}}
    </div>
    @enderror
</div>
<div class="form-group">
    <label for="bio">Bio</label>
    <textarea name="bio" id="bio" cols="30" rows="10" class="form-control"></textarea>
    @error('bio')
    <div class="alert alert-danger">
        {{$message}}
    </div>
    @enderror
</div>
<div class="form-group">
    <button type="submit" class="btn btn-success">Tambah</button>
</div>
</form>
@endsection

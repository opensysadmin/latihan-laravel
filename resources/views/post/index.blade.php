@extends('layout.master')
@section('judul', 'Tambah Post')
@section('konten')
    <a href="/post/create" class="btn btn-primary mb-3">Tambah</a>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Body</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($post as $key=>$value)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $value->title }}</td>
                    <td>{{ $value->body }}</td>
                    <td>
                        <div class="row">
                            <a href="/post/{{ $value->id }}" class="btn btn-info mx-1">Show</a>
                            <a href="/post/{{ $value->id }}/edit" class="btn btn-primary mx-1">Edit</a>
                            <form action="/post/{{ $value->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger mx-1" value="Delete">
                            </form>
                        </div>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection

@extends('layout.master')
@section('judul', 'Show Post')
@section('konten')
<h2>Show Post {{$post->id}}</h2>
<h4>{{$post->title}}</h4>
<p>{{$post->body}}</p>
@endsection

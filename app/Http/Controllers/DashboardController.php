<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //

    public function master()
    {
        return view('layout.master');
    }

    public function table()
    {
        return view('table');
    }

    public function datatables()
    {
        return view('datatables');
    }
}
